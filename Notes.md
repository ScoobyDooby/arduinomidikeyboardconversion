# Converting non-midi keyboard to midi keyboard

The CasioCTK-520L appears to use multiplexing to check the state of each key one at a time. The high logic level appears to be Vcc of about 5V and the low level is the diode drop of about 0.6V. This ties in with standard CMOS logic level voltages of LOW below 1/3 Vcc (1.7V) and HIGH above 2/3 Vcc (~3.3V).

There are 16 inputs to the CTK-520Ls microprocessor which presumably gives a 8x8 multiplex giving 64 addressable inputs which is enough for the 61 keys.

The arduino Nano has 14 digital pins that can be used as input or output. It also has 8 analog inputs. Analog pins 6 and 7 cannot be used as digital pins.

Digital pin 1 is used for TX and cannot be used at the same time as the serial port.

Digital pin 13 is attached to an onboard LED and resistor. Setting this pin to INPUT_PULLUP results in a pin voltage of about 1.7V instead of 5V. Makes sense to either disconnect the LED or use this pin as the MIDI output.

This leaves us with 17 pins we can use which is just enough for the 16 multiplexing pins and the midi output pin.
