#include <Arduino.h>
#include <avr/power.h>

// #define DEBUG

// MIDI out is pin 1. We can use Serial.write() to write midi commands to pin 1;
// Using analog pins for inputs didn't seem to work. Working fine as outputs.

#define OUT0 11 // Digital pin 11 PORTB
#define OUT1 12 // Digital pin 12 PORTB
#define OUT2 A0 // Analog pin A0 PORTC
#define OUT3 A1 // Analog pin A1 PORTC
#define OUT4 A2 // Analog pin A2 PORTC
#define OUT5 A3 // Analog pin A3 PORTC
#define OUT6 A4 // Analog pin A4 PORTC
#define OUT7 A5 // Analog pin A5 PORTC

#define IN0 3 // Digital pin 3 PORTD
#define IN1 4 // Digital pin 4 PORTD
#define IN2 5 // Digital pin 5 PORTD
#define IN3 6 // Digital pin 6 PORTD
#define IN4 7 // Digital pin 7 PORTD
#define IN5 8 // Digital pin 8 PORTB
#define IN6 9 // Digital pin 9 PORTB
#define IN7 10 // Digital pin 10 PORTB

#define DEBOUNCE_PERIOD_MILLIS 50

#define PORTD_ADDRESS 0x2B
#define PIND_ADDRESS 0x29
#define PORTB_ADDRESS 0x25
#define PINB_ADDRESS 0x23
#define PORTC_ADDRESS 0x28 
#define PINC_ADDRESS 0x26

/* MIDI codes
* The first byte of a MIDI message is the status byte. It is the only byte that
* has bit 7 set. This identifies the start of MIDI message.
* 
* Status bytes between 0x80 and 0xEF are messages on particular channels. The 
* first four bits indicate the type of message (e.g. note on) and the last four
* bits indicate the channel.
* 
* After the status byte can come data bytes depending on the message type.
* 
* Note off 
*   128 possible note pitches and 128 possible velocities
*   Status 0x8x, Data note number 0x00 to 0x7F, Velocity 0x00 to 0x7F.
*   
* Note on
*   128 possible note pitches and 128 possible velocities.
*   Should always be followed by a note off at some point.
*   Status 0x9x, Data: note numberr 0x00 to 0x7F, Velocity 0x00 to 0x7F
*
* Aftertouch
*  How hard the key is pressed down while it is being played.
*  128 possible pressure values.
*  Status 0xAx, Data: note number 0x00 to 0x7f, pressure 0x00 to 0x7F
*/
#define NOTE_ON 0x90 // MIDI Note on event on channel 1
#define NOTE_OFF 0x80 // MIDI Note off event on channel 1
#define ON_VELOCITY 0x7F
#define OFF_VELOCITY 0x7F

#define C2              0x24
#define C_SHARP2_DFLAT2 0x25
#define D2              0x26
#define D_SHARP2_EFLAT2 0x27
#define E2              0x28
#define F2              0x29
#define F_SHARP2_GFLAT2 0x2A
#define G2              0x2B
#define G_SHARP2_AFLAT2 0x2C
#define A2_MIDI         0x2D
#define A_SHARP2_FLATb2 0x2E
#define B2              0x2F
#define C3              0x30
#define C_SHARP3_DFLAT3 0x31
#define D3              0x32
#define D_SHARP3_EFLAT3 0x33
#define E3              0x34
#define F3              0x35
#define F_SHARP3_GFLAT3 0x36
#define G3              0x37
#define G_SHARP3_AFLAT3 0x38
#define A3_MIDI         0x39
#define A_SHARP3_FLAT3  0x3A
#define B3              0x3B
#define C4              0x3C
#define C_SHARP4_DFLAT4 0x3D
#define D4              0x3E
#define D_SHARP4_EFLAT4 0x3F
#define E4              0x40
#define F4              0x41
#define F_SHARP4_GFLAT4 0x42
#define G4              0x43
#define G_SHARP4_AFLAT4 0x44
#define A4_MIDI         0x45
#define A_SHARP4_FLAT4  0x46
#define B4              0x47
#define C5              0x48
#define C_SHARP5_DFLAT5 0x49
#define D5              0x4A
#define D_SHARP5_EFLAT5 0x4B
#define E5              0x4C
#define F5              0x4D
#define F_SHARP5_GFLAT5 0x4E
#define G5              0x4F
#define G_SHARP5_AFLAT5 0x50
#define A5_MIDI         0x51
#define A_SHARP5_FLAT5  0x52
#define B5              0x53
#define C6              0x54
#define C_SHARP6_DFLAT6 0x55
#define D6              0x56
#define D_SHARP6_EFLAT6 0x57
#define E6              0x58
#define F6              0x59
#define F_SHARP6_GFLAT6 0x5A
#define G6              0x5B
#define G_SHARP6_AFLAT6 0x5C
#define A6_MIDI         0x5D
#define A_SHARP6_FLAT6  0x5E
#define B6              0x5F
#define C7              0x60

uint16_t getInputPortAddress(uint16_t);
uint16_t getRegisterBit(uint16_t);
uint16_t getOutputPortAddress(uint16_t);
void updateKey(uint16_t,uint16_t);

// TODO we may need to debounce the inputs.
class Key {
  public:
    int midiNoteNumber;
    bool isPressed = false;
    long debounceStartTime = 0;

    Key(int midiNoteNumber){
    this->midiNoteNumber = midiNoteNumber;
    }
};

uint16_t inputPortAddresses[8] {
  getInputPortAddress(IN0),
  getInputPortAddress(IN1),
  getInputPortAddress(IN2),
  getInputPortAddress(IN3),
  getInputPortAddress(IN4),
  getInputPortAddress(IN5),
  getInputPortAddress(IN6),
  getInputPortAddress(IN7),
};

uint16_t inputRegisterBits[8] { 
  getRegisterBit(IN0),
  getRegisterBit(IN1), 
  getRegisterBit(IN2), 
  getRegisterBit(IN3), 
  getRegisterBit(IN4), 
  getRegisterBit(IN5), 
  getRegisterBit(IN6), 
  getRegisterBit(IN7),
};

uint16_t outputPortAddresses[8] {
  getOutputPortAddress(OUT0),
  getOutputPortAddress(OUT1), 
  getOutputPortAddress(OUT2), 
  getOutputPortAddress(OUT3), 
  getOutputPortAddress(OUT4), 
  getOutputPortAddress(OUT5), 
  getOutputPortAddress(OUT6), 
  getOutputPortAddress(OUT7),
};
uint16_t outputRegisterBits[8]  { 
  getRegisterBit(OUT0),
  getRegisterBit(OUT1), 
  getRegisterBit(OUT2), 
  getRegisterBit(OUT3), 
  getRegisterBit(OUT4), 
  getRegisterBit(OUT5), 
  getRegisterBit(OUT6), 
  getRegisterBit(OUT7),
};

Key keys[8][8] = {
  {Key(C2),              Key(C_SHARP2_DFLAT2), Key(D2),              Key(D_SHARP2_EFLAT2), Key(E2),              Key(F2),              Key(F_SHARP2_GFLAT2), Key(G2)},
  {Key(G_SHARP2_AFLAT2), Key(A2_MIDI),         Key(A_SHARP2_FLATb2), Key(B2),              Key(C3),              Key(C_SHARP3_DFLAT3), Key(D3),              Key(D_SHARP3_EFLAT3)},
  {Key(E3),              Key(F3),              Key(F_SHARP3_GFLAT3), Key(G3),              Key(G_SHARP3_AFLAT3), Key(A3_MIDI),         Key(A_SHARP3_FLAT3),  Key(B3)},
  {Key(C4),              Key(C_SHARP4_DFLAT4), Key(D4),              Key(D_SHARP4_EFLAT4), Key(E4),              Key(F4),              Key(F_SHARP4_GFLAT4), Key(G4)},
  {Key(G_SHARP4_AFLAT4), Key(A4_MIDI),         Key(A_SHARP4_FLAT4),  Key(B4),              Key(C5),              Key(C_SHARP5_DFLAT5), Key(D5),              Key(D_SHARP5_EFLAT5)},
  {Key(E5),              Key(F5),              Key(F_SHARP5_GFLAT5), Key(G5),              Key(G_SHARP5_AFLAT5), Key(A5_MIDI),         Key(A_SHARP5_FLAT5),  Key(B5)},
  {Key(C6),              Key(C_SHARP6_DFLAT6), Key(D6),              Key(D_SHARP6_EFLAT6), Key(E6),              Key(F6),              Key(F_SHARP6_GFLAT6), Key(G6)}, 
  {Key(G_SHARP6_AFLAT6), Key(A6_MIDI),         Key(A_SHARP6_FLAT6),  Key(B6),              Key(C7),              Key(NULL),            Key(NULL),                 Key(NULL)}
};


void setup() {

  //clock_prescale_set(clock_div_1);
  #ifndef DEBUG
  // MIDI uses 31.25k baud rate.
  Serial.begin(31250);
  #else
  // Baud rate for testing
  Serial.begin(38400);
  Serial.println("Debugging");

  #if ((F_CPU) == 1000000)
  #warning F_CPU = 1MHz
  Serial.println("1MHz");
  #elif ((F_CPU) == 2000000)
  #warning F_CPU = 2MHz
  Serial.println("2MHz");
  #elif ((F_CPU) == 4000000)
  #warning F_CPU = 4MHz
  Serial.println("4MHz");
  #elif ((F_CPU) == 8000000)
  #warning F_CPU = 8MHz
  Serial.println("8MHz");
  #elif ((F_CPU) == 16000000)
  #warning F_CPU = 16MHz
  Serial.println("16MHz");
  #elif ((F_CPU) == 32000000)
  #warning F_CPU = 32MHz
  Serial.println("32MHz");
  #endif

  #endif

  pinMode(IN0, INPUT_PULLUP);
  pinMode(IN1, INPUT_PULLUP);
  pinMode(IN2, INPUT_PULLUP);
  pinMode(IN3, INPUT_PULLUP);
  pinMode(IN4, INPUT_PULLUP);
  pinMode(IN5, INPUT_PULLUP);
  pinMode(IN7, INPUT_PULLUP);
  pinMode(IN7, INPUT_PULLUP);

  pinMode(OUT0, OUTPUT);
  pinMode(OUT1, OUTPUT);
  pinMode(OUT2, OUTPUT);
  pinMode(OUT3, OUTPUT);
  pinMode(OUT4, OUTPUT);
  pinMode(OUT5, OUTPUT);
  pinMode(OUT6, OUTPUT);
  pinMode(OUT7, OUTPUT);

  digitalWrite(OUT0,HIGH);
  digitalWrite(OUT1,HIGH);
  digitalWrite(OUT2,HIGH);
  digitalWrite(OUT3,HIGH);
  digitalWrite(OUT4,HIGH);
  digitalWrite(OUT5,HIGH);
  digitalWrite(OUT6,HIGH);
  digitalWrite(OUT7,HIGH);
  
}

void loop() {

  //long startTime = micros();
  for(int input = 0; input < 7; input++)
  {
    for(int output = 0; output < 8; output++)
    {
      updateKey(input,output);
    }
  }

  for(int output = 0; output < 5; output++)
  {
    updateKey(7, output);
  }
  //Serial.println((micros() - startTime) / 1000.0);
}

void updateKey(uint16_t input, uint16_t output)
{
  
  Key &key = keys[input][output];

  // Set output pin low
  uint16_t outputPortAddress = outputPortAddresses[output];
  _MMIO_BYTE(outputPortAddress) = _MMIO_BYTE(outputPortAddress) ^ outputRegisterBits[output];
  uint16_t registerBit = inputRegisterBits[input];

  bool keyPressed = !((_MMIO_BYTE(inputPortAddresses[input]) & registerBit) == registerBit);
  bool keyChanged = keyPressed != key.isPressed;
  //Serial.print("keyPressed: "); Serial.print(keyPressed); Serial.print(", keyChanged: " ); Serial.println(keyChanged);
  if(keyChanged)
  {
    long time = millis();
    long period = time - key.debounceStartTime;
    // Serial.print("time: "); Serial.print(time); Serial.print(" key.debounceStartTime: "); Serial.print(key.debounceStartTime);
    // Serial.print(" period: "); Serial.print(period);
    bool inDelayPeriod = period < DEBOUNCE_PERIOD_MILLIS;
    // Serial.print(" inDelayPeriod: "); Serial.println(inDelayPeriod);
    if(!inDelayPeriod)
    {
      key.debounceStartTime = time;

      if(keyPressed)
      {
         if(!key.isPressed)
        {
          key.isPressed = true;
          #ifdef DEBUG
          Serial.print("NoteOn ");
          Serial.println(key.midiNoteNumber, HEX);
          #else
          Serial.write(NOTE_ON);
          Serial.write(key.midiNoteNumber);
          Serial.write(ON_VELOCITY);
          #endif
        }
      }
      else
      {
        // Key not pressed
        if(key.isPressed)
        {
          key.isPressed = false;
          #ifdef DEBUG
          Serial.print("  NoteOff ");
          Serial.println(key.midiNoteNumber, HEX);
          #else
          Serial.write(NOTE_OFF);
          Serial.write(key.midiNoteNumber);
          Serial.write(ON_VELOCITY);
          #endif
        }
       
      }
    }
  }

  // Set output pin high.
  _MMIO_BYTE(outputPortAddress) = 0xFF;
}


uint16_t getInputPortAddress(uint16_t pin)
{
  if(pin >= 0 && pin <=7)
  {
    return PIND_ADDRESS;
  }

  if(pin >=8 && pin <= 13)
  {
    return PINB_ADDRESS;
  }

  return PINC_ADDRESS;
}

uint16_t getOutputPortAddress(uint16_t pin)
{
  if(pin >= 0 && pin <=7)
  {
    return PORTD_ADDRESS;
  }

  if(pin >=8 && pin <= 13)
  {
    return PORTB_ADDRESS;
  }

  return PORTC_ADDRESS;
}

uint16_t getRegisterBit(uint16_t pin)
{
  // Digital pins 0 to 7
  if(pin >= 0 && pin <=7)
  {
    return 1 << pin;
  }
  // Digital pins 0 to 8
  if(pin >=8 && pin <= 13)
  {
    return 1 << (pin - 8);
  }
  // Analog pin A0 = 14  
  return 1 << (pin - 14) ;
}
